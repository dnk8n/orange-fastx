import logging

from Orange.widgets.data import owfile
from Orange.widgets.utils.widgetpreview import WidgetPreview
from Orange.data.io import FileFormat
from Bio import SeqIO
from Bio.SeqIO.QualityIO import FastqGeneralIterator as Fgi

log = logging.getLogger(__name__)


class FastaReader(FileFormat):
    """Reader for fasta files"""

    EXTENSIONS = ('.fasta',)
    DESCRIPTION = 'Fasta'

    def read(self):
        fasta_seqs = SeqIO.parse(self.filename, 'fasta')
        data = [[fasta.id, str(fasta.seq)] for fasta in fasta_seqs]
        return self.data_table(data=data, headers=[['id', 'seq']])


class FastqReader(FileFormat):
    """Reader for fastq files"""

    EXTENSIONS = ('.fastq',)
    DESCRIPTION = 'Fastq'

    def read(self):
        with open(self.filename, 'rU') as handle:
            data = [[id_, seq, phred] for id_, seq, phred in Fgi(handle)]
        return self.data_table(data=data, headers=[['id', 'seq', 'phred']])


if __name__ == '__main__':
    WidgetPreview(owfile.OWFile).run()
