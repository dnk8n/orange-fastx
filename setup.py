from setuptools import setup

setup(
    name='orange-fastx',
    version='0.0.2',
    description='Extends the Orange File widget to also read fasta/fastq',
    author='Dean Kayton',
    author_email='dean.kayton@uct.ac.za',
    packages=['orange_fastx'],
    entry_points={'orange.widgets': 'Data=orange_fastx'},
    install_requires=['biopython']
)
