## Orange-Fastx

### What it does
This is an Orange extension which modifies its already existing file widget to handle [fasta](https://en.wikipedia.org/wiki/FASTA_format) and [fastq](https://en.wikipedia.org/wiki/FASTQ_format) files

### Dependencies
[Orange](http://orange.biolab.si/)

### Installation
`pip install -e git+https://gitlab.com/dnk8n/orange-fastx#egg=orange_fastx` from the same virtual environment (if applicable) as Orange

### Removal
`pip uninstall orange-fastx`
